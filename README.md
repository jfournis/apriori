# A COMPSs implementation of the Apriori algorithm

Implementation in COMPSs of an algorithm to solve the "Frequent Itemize Mining" problem.
Find information here (http://fimi.uantwerpen.be).

## Apriori algorithm

### What does the the algorithm do?

Given a data set, the algorithm find out which combinations of item appears frequently.
For instance, the data is a hypothetical data set from a night store in which customers mainly buy wine and cheese or beer and potato chips.


| Transaction | Product                          |
|-------|----------------------------------|
| 1     | beer, wine, cheese               |
| 2     | beer, potato, chips              |
| 3     | wine, egg, flour, butter, cheese |
| 4     | wine, cheese                     |
| 5     | potato, chips                    |
| 6     | egg, flour, butter, wine, cheese |



The idea of the algorithm is to find the set of items that are frequent, that is to say that appears more than an given threeshold.
Let's say that 4 is the threeshold for the data set below, then  wine, cheese is frequent


### Overview of the algorithm


The Algorithm iteratively computes the set <img src="https://render.githubusercontent.com/render/math?math={L_k}"> of frequent itemsets of length <img src="https://render.githubusercontent.com/render/math?math=k">.
It proceeds in two steps:
- It generates a set <img src="https://render.githubusercontent.com/render/math?math={ C_k }"> of candidates for a frequent itemset of length <img src="https://render.githubusercontent.com/render/math?math={k}">.
- It filters the items from <img src="https://render.githubusercontent.com/render/math?math={C_k}"> by counting their number of occurrences in the database.
### The generation function
For generation, we use that all subsets of a frequent set must be frequent. 
Given two sets of length <img src="https://render.githubusercontent.com/render/math?math={n - 1}"> , having <img src="https://render.githubusercontent.com/render/math?math={n-2}">  elements in common, we can construct a set of length <img src="https://render.githubusercontent.com/render/math?math={n}">  by keeping the <img src="https://render.githubusercontent.com/render/math?math={n - 2}"> elements shared by the two subsets and adding the <img src="https://render.githubusercontent.com/render/math?math={2}"> different letters of the two sets.


### The filter function
Given a set of words W, we need to count their occurrence in a text. For this we have adapt the example of counting words in a text (https://compss-doc.readthedocs.io/en/stable/Sections/09_PyCOMPSs_Notebooks/hands-on/11_Wordcount_Exercise.html?highlight=wordcount).
To allow parallelism the database is divided in blocks that are treated separately and merged afterwards.
Thus we call two differents COMPSs tasks:
- a counting task
- a merging task



## Installation

Install COMPSs


I advice you to use python3 to avoid issues

    update-alternatives --install /usr/bin/python python /usr/bin/python3 1

    pip install pycompss
    

## Experiments Description and trace analysis
We have made several experiments to test our algorithm. We present our results for a dataset representing chess board.
The items are the 64 cases of the board and an record reprensent the case which are occupied.
There is 3196 records and the size of our database is 339 kB. We have taken as threeshold 2700. This lead to the generation of frequents itemset :

| size of frequent itemsets | Number of frequent itemsets | Number of candidates |
|---------------------------|-----------------------------|----------------------|
| 1                         | 17                          |                      |
| 2                         | 114                         | 136                  |
| 3                         | 391                         | 465                  |
| 4                         | 775                         | 917                  |
| 5                         | 923                         | 1139                 |
| 6                         | 643                         | 889                  |
| 7                         | 237                         | 402                  |
| 8                         | 34                          | 94                   |
| 9                         | 0                           | 8                    |

We have deploy our application on Grid5000 platform and Marenostrum4 supercomputer, using 1,10,100 and 1000 workers.
The trace that we obtained are shown below.


## Usage
    runcompss src/frequents.py < filename > < nb_of_cores >  < minimum occurrence threshold >

### Run it sequentially
    python3 src/frequents.py database/chess.txt 4 2700
### Run it locally using 4 cores
    runcompss -t src/frequents.py database/chess.txt 4 2700 

### Deploy it on Grid5000
    
    git clone git@gitlab.inria.fr:jfournis/compss-deployment.git --recursive
    cd compss-deployment
    ./install.sh
    source $COMPSsDeployment/create.sh` $COMPSsDeployment/examples/apriori $COMPSsDeployment/examples/layers_services/apriori_layers_services.yaml
    e2clab deploy scenario_dir artifacts_dir 




### Deploy it on MareNostrum4

Copy this directory and send it to MareNostrum4

    git clone git@gitlab.inria.fr:jfournis/apriori.git 

Launch the algorithm on MareNostrum4

    ssh mn1.bsc.es "rm -rf apriori" && rsync -av -e ssh --exclude '.git' --exclude '.idea' --exclude '__pycache__' ~/apriori mn1.bsc.es:~/ && export cmd="pycompss job submit  --exec_time=15   --num_nodes=1  --cpus_per_node=10  --master_working_dir=.   --worker_working_dir=shared_disk    --log_level=debug  --worker_in_master_cpus=10 --tracing=true --lang=python frequents.py ~/apriori/database/chess.txt 10 2700" && export JOB_ID=$(ssh mn1.bsc.es "export COMPSS_PYTHON_VERSION=3 && module load COMPSs/2.10 && cd apriori/src && $cmd" | tail -n 1 | awk -F ' ' '{print $3}') 
   
Make a small description of your experiment

     mkdir ~/.COMPSs/$JOB_ID && echo "Describe a bit your experiment :)" && read -r message && echo $message >>  ~/.COMPSs/$JOB_ID/desc

Check the results

    ssh mn1.bsc.es "squeue"
    ssh mn1.bsc.es "cd apriori/src && cat compss-$JOB_ID.err"
    ssh mn1.bsc.es "cd apriori/src && cat compss-$JOB_ID.out"

Fetch the results

    scp -r mn1.bsc.es:/home/bsc19/bsc19026/.COMPSs/$JOB_ID ~/.COMPSs
    scp -r mn1.bsc.es:apriori/src/compss-$JOB_ID.out ~/.COMPSs/$JOB_ID



Get information about experiments (usefull for retrieving the experiments)

    for f in $(ls ~/.COMPSs/); do  echo "Processing dir $f" && cat ~/.COMPSs/$f/desc | grep "experiment"; done

### Display the trace generated

Install paraver (https://tools.bsc.es/paraver/downloads)

    export PARAVER=~/wxparaver-4.10.0-Linux_x86_64/bin/wxparaver
    $PARAVER start






