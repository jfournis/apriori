#!/usr/bin/python

import sys


def hashable(w):
    res = 0
    mul = 1
    for l in w:
        res += mul * l
        mul = mul * 76
    return res

def convert(word):
    return list(map(int, word.split()))


def revert(word):
    base=76
    res = []
    j = word
    while j > base:
        res.append(j % base)
        j = j // base
    res.append(j)
    return res



def mergeReduce(function, data):
    """ Apply function cumulatively to the items of data,
        from left to right in binary tree structure, so as to
        reduce the data to a single value.
    :param function: function to apply to reduce data
    :param data: List of items to be reduced
    :return: result of reduce the data to a single value
    """
    from collections import deque
    q = deque(list(range(len(data))))
    while len(q):
        x = q.popleft()
        if len(q):
            y = q.popleft()
            data[x] = function(data[x], data[y])
            q.append(x)
        else:
            return data[x]



def read_word(file_object):
    for word in file_object:
        yield word[:-1]


def read_word_by_word(fp, sizeBlock):
    """Lazy function (generator) to read a file piece by piece in
    chunks of size approx sizeBlock"""
    data = open(fp)
    block = []
    for word in read_word(data):
        word = convert(word)
        block.append(word)
        if sys.getsizeof(block) > sizeBlock:
            yield block
            block = []
    if block:
        yield block
