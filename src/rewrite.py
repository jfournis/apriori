from pycompss.api.task import task

data = [
    'Albania', '28748', 'AL', 'ALB', 'Algeria', '2381741', 'DZ', 'DZA','American Samoa', 'AO', 'AGO'
]
letters=['8','4','2']

def clean(w,letters):
    f = ""
    for l in w.split(' '):
        if l in letters:
            f += l
            f += ' '
    return f

@task()
def rewrite(fp, fout, letters):
    data = open(fp)
    result = open(fout,"a")
    for word in data:
        w = clean(word,letters)
        if w != "":
            result.write(w)
            result.write("\n")

