import queue
from util import *
from pycompss.api.task import task

class TrieNode:
    """A node in the trie structure"""

    def __init__(self, char):
        # the character stored in this node
        self.char = char

        # a counter for counting occurence
        self.counter = 0

        # a list of child nodes
        self.children = []


class Trie(object):
    """The trie object"""


    def __init__(self):
        """
        The trie has at least the root node.
        The root node does not store any character
        """
        self.root = TrieNode(0)

    def get_root(self):
        return self.root

    def insert(self, word):
        """Insert a word into the trie"""
        node = self.root
        # Loop through each character in the word
        # Check if there is no child containing the character, create a new child for the current node
        for char in word:
            i=0
            n=len(node.children)
            insert=False
            while (not insert) and (i!= n) and (char <= node.children[i].char):
                if char == node.children[i].char:
                    node = node.children[i]
                    insert=True
                i+=1
            if not insert:
                new_node = TrieNode(char)
                nodes = []
                for j in range(i):
                    nodes.append(node.children[j])
                nodes.append(new_node)
                for j in range(i,n):
                    nodes.append(node.children[j])
                node.children = nodes
                node = new_node


    def bfs(self):
        FIFO = queue.Queue()
        FIFO.put(self.root)
        while not FIFO.empty():
            node = FIFO.get()
            for c in node.children:
                FIFO.put(node.children[c])


    def dfs(self):
        LIFO = queue.LifoQueue()
        LIFO.put(self.root)
        while not LIFO.empty():
            node = LIFO.get()
            for c in node.children:
                LIFO.put(node.children[c])

    def to_list(self):
        FIFO = queue.Queue()
        FIFO.put((self.root,''))
        while not FIFO.empty():
            node,s = FIFO.get()
            if len(node.children) == 0:
                if node.counter > 0:
                    print(s+str(node.counter))

            else:
                for c in node.children:
                    FIFO.put((c,s+c.char))

    def to_dict(self):
        res = {}
        FIFO = queue.LifoQueue()
        FIFO.put((self.get_root(),[]))
        while not FIFO.empty():
            node,s = FIFO.get()
            if len(node.children) == 0:
                    res[hashable(s)]=node.counter
            else:
                for c in node.children:
                    sc = s.copy()
                    sc.append(c.char)
                    FIFO.put((c,sc))
        return res


def occurence(trie,w):
    FIFO = queue.LifoQueue()
    for c in trie.get_root().children:
        FIFO.put((c,0))
    while not FIFO.empty():
        node,i = FIFO.get()
        if len(node.children) == 0:
            if node.char in w[i:]:
                node.counter+=1
        else:
            if node.char==w[i]:
                for child in node.children:
                    FIFO.put((child,i+1))
            if node.char>w[i]:
                FIFO.put((node, i+1))

@task()
def createTrie(words):
    trie = Trie()
    for w in words:
        trie.insert(w)
    return trie

@task()
def wordCount(data,trie):
    for entry in data:
        occurence(trie,entry)
    return trie
                        
@task(returns=list)
def frequentWords(trie, minSupport):
    res = []
    FIFO = queue.LifoQueue()
    FIFO.put((trie.get_root(),[]))
    while not FIFO.empty():
        node,s = FIFO.get()
        if len(node.children) == 0:
            if node.counter >= minSupport:
                res.append(s)
        else:
            for c in node.children:
                sc = s.copy()
                sc.append(c.char)
                FIFO.put((c,sc))
    return res


@task(priority=True)
def merge_two_tries(t1,t2):
    FIFO = queue.Queue()
    FIFO.put((t1.get_root(), t2.get_root()))
    while not FIFO.empty():
        node1, node2 = FIFO.get()
        if len(node1.children) == 0:
            node1.counter += node2.counter
        else:
            for i in range(len(node1.children)):
                FIFO.put((node1.children[i], node2.children[i]))
    return t1


def Triefilter(words, pathFile, minSupport, sizeBlock):
    partialResult = []
    for block in read_word_by_word(pathFile, sizeBlock):
        partialResult.append(wordCount(block, createTrie(words)))
    words = mergeReduce(merge_two_tries, partialResult)
    return frequentWords(words, minSupport)

# t = Trie()
# t.insert([5,9])
#
#
#
#
# occurence(t,[1,2,4,6,9])
#
# print(t.to_dict())
#
# occurence(t,[1,3,5,7,8])