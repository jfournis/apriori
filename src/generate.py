#!/usr/bin/python

from pycompss.api.task import task
import sys
from util import mergeReduce
from pycompss.api.api import compss_wait_on


words = [[1,2,3,4], [1,2,3,5], [1,2,4,5], [1,3,4,5],[2,3,4,5]]

def split(l):
    """ Construct two list given a list of words w1,...,wk, where each word has the same length n
    prefix: the list of w1[1:],...,wk[1:] without repetition
    suffixx: the list of list of the last letters of the words, the suffix of the words having the same prefix are in the same list [w1[n],..],...[..,wk[n]]
    """
    prefix = []
    suffix = []
    suffixx = []
    p = l[0][:-1]
    s = l[0][-1]
    prefix.append(p)
    suffix.append(s)
    for s in l[1:]:
        if p == s[:-1]:
            suffix.append(s[-1])
        else:
            p = s[:-1]
            s = s[-1]
            suffixx.append(suffix)
            suffix = [s]
            prefix.append(p)
    suffixx.append(suffix)
    return prefix, suffixx


def read_list_block_by_block(list,sizeBlock):
    """Lazy function (generator) to read a list piece by piece in
    chunks of size approx sizeBlock"""
    prefix, suffixx = split(list)
    blockp, blocks = [], []
    for i in range(len(prefix)):
        blockp.append(prefix[i])
        blocks.append(suffixx[i])
        if sys.getsizeof(blockp) > sizeBlock:
            yield (blockp,blocks)
            blockp, blocks = [], []
    if blockp:
        yield (blockp,blocks)


def merge(prefix, suffixx):
    """
         prefix a list of set of elements
         suffixx: a list of list of elements
         generate every combinaison of set of two elements given the elements in a list
         for each combinaison generate with element i of suffixx appending to element i of prefix
    """
    res = []
    for i in range(len(prefix)):
        p = prefix[i]
        sl = suffixx[i]
        if len(sl) >= 2:
            for i in range(len(sl)):
                for j in range(i+1,len(sl)):
                    w = p.copy()
                    w.append(sl[i])
                    w.append(sl[j])
                    res.append(w)
    return res

@task(returns=list)
def generate(l):
    prefix, suffixx = split(l)
    return merge(prefix, suffixx)

def is_subworld(u,w):
    n=len(u)
    for i in range(n):
        if w[i] != u[i]:
            return  w[i+1:] == u[i:]
    return True

def count_subworlds(w,l):
    count=0
    for u in l:
        if is_subworld(u,w):
            count+=1
    return count

def keep(w,l):
    return count_subworlds(w,l) == len(w)



@task(returns=list)
def candidates(block, l):
    res=[]
    prefix, suffixx = block
    print(prefix,suffixx)
    cdts = merge(prefix, suffixx)
    print(cdts)
    for w in cdts:
        if keep(w,l):
            res.append(w)
    return res


def merge_two_lists(l1,l2):
    return l1+l2

def parallel_candidates(l,sizeBlock):
    partialResult=[]
    for block in read_list_block_by_block(l, sizeBlock):
        partialResult.append(candidates(block,l))
    partialResult=compss_wait_on(partialResult)
    cdts = mergeReduce(merge_two_lists, partialResult)
    return cdts


#
# print(is_subworld([1,2,3], [1,2,3,5]))
# print(is_subworld([1,2,5], [1,2,3,5]))
# print(is_subworld([2,3,5], [1,2,3,5]))
# print(is_subworld([2,3,4], [1,2,3,5]))
# print(count_subworlds([1,2,3,5,6],words))