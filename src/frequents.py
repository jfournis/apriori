#!/usr/bin/python

import os
import time

from generate import *
from filter import *
from write import *
from rewrite import *
from Trie import *



if __name__ == "__main__":

    from pycompss.api.api import compss_wait_on

    # Start counting time...
    start_time = time.time()

    pathFile = sys.argv[1]
    NbThreads = int(sys.argv[2])
    with open(pathFile, 'r') as fp:
        for count, line in enumerate(fp):
            pass
    nblines = count + 1
    print('nblines: '+str(nblines))
    sizeBlock = nblines // NbThreads
    # print('sizeBlock: '+sizeBlock)
    minSupport = int(sys.argv[3])

    csvdata = []
    length_of_itemsets=1
    stats=[length_of_itemsets]

    words = generate_frequents_letters(pathFile, minSupport, sizeBlock)
    words = compss_wait_on(words)


    stats.append("")
    stats.append(str(len(words)))
    csvdata.append(stats)

    print('letters: '+str(words))
    print('letters: '+str(len(words)))
    #rewrite(pathFile,'/tmp/database_tmp',[str(l[0]) for l in words])



    while words != []:
        #words = generate(words)
        words = parallel_candidates(words,sizeBlock)
        words = compss_wait_on(words)

        length_of_itemsets += 1
        stats = [length_of_itemsets]
        stats.append(str(len(words)))
        #print('generate: '+str(len(words)))

        #words = filter(words, pathfile, minSupport, sizeBlock)
        words = filter(words, pathFile, minSupport, sizeBlock)
        words = compss_wait_on(words)

        stats.append(str(len(words)))
        csvdata.append(stats)
        #print('filter: '+str(len(words)))

    elapsed_time = time.time() - start_time
    write_csv(['elapsed_time'],[[elapsed_time]])
    #write_csv(['length of itemsets','Number of candidates','Number of frequent itemsets'], csvdata)
    #os.remove('/tmp/database_tmp')



    print("Elapsed Time (s): " + str(elapsed_time))

