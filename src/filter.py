from util import *
from pycompss.api.task import task


@task(returns=list)
def frequentWords(D, minSupport):
    res = []
    for w, count in D.items():
        if count >= minSupport:
            res.append(revert(w))
    return res






def subword(sub, entry):
    i = -1
    for a in sub:
        i += 1
        if not (a in entry[i:]): return False
        j = entry[i:].index(a)
        i = i + j
    return True

@task(returns=dict)
def wordCount(data, words):
    """ Construct a frequency word dictorionary from a list of words.
    :param data: a list of words
    :return: a dictionary where key=word and value=#appearances
    """

    partialResult = {}
    for entry in data:
        for w in words:
            if subword(w, entry):
                if hashable(w) in partialResult:
                    partialResult[hashable(w)] += 1
                else:
                    partialResult[hashable(w)] = 1
    return partialResult



@task(returns=dict)
def lettersCount(data):

    partialResult = {}
    for entry in data:
        for w in entry:
            if hashable([w]) in partialResult:
                partialResult[hashable([w])] += 1
            else:
                partialResult[hashable([w])] = 1
    return partialResult



@task(returns=dict, priority=True)
def merge_two_dicts(dic1, dic2):
    """ Update a dictionary with another dictionary.
    :param dic1: first dictionary
    :param dic2: second dictionary
    :return: dic1+=dic2
    """
    for k in dic2:
        if k in dic1:
            dic1[k] += dic2[k]
        else:
            dic1[k] = dic2[k]
    return dic1

def generate_frequents_letters(pathFile, minSupport, sizeBlock):
    partialResult = []
    for block in read_word_by_word(pathFile, sizeBlock):
        partialResult.append(lettersCount(block))
    words = mergeReduce(merge_two_dicts, partialResult)
    return frequentWords(words, minSupport)


def filter(words, pathFile, minSupport, sizeBlock):
    partialResult = []
    for block in read_word_by_word(pathFile, sizeBlock):
        partialResult.append(wordCount(block, words))
    words = mergeReduce(merge_two_dicts, partialResult)
    return frequentWords(words, minSupport)